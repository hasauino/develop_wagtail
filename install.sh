#!/usr/bin/env bash

# Fail if any command fails.
set -e

# Update APT database
sudo apt-get update -y

sudo apt-get install -y build-essential
sudo apt-get install -y python3-dev python3-venv
sudo apt-get install -y libjpeg-dev libtiff-dev zlib1g-dev libfreetype6-dev liblcms2-dev
sudo apt-get install -y libenchant-dev


cd ./wagtail_io

pip install -e  '.[testing,docs]' --force-reinstall


