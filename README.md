# develop_wagtail

This repository prepares a development environment for our local [wagtail_io](https://git.inf.h-brs.de/wagtail/wagtail_io) along side an existing [infoportal](https://git.inf.h-brs.de/wagtail/infoportal) instance. So if you want to make changes  on [wagtail_io](https://git.inf.h-brs.de/wagtail/wagtail_io) while you are developing on [infoportal](https://git.inf.h-brs.de/wagtail/infoportal), this repository is just for that.

This repository is based on [docker-wagtail-develop](https://github.com/wagtail/docker-wagtail-develop).

## Requirements

- [Docker](https://www.docker.com/)
- [Docker-compose](https://docs.docker.com/compose/)

Docker and docker-compose are used to compile static files (`.scss` to `.css`, and minify `.js` files) of Wagtail.



## How to Use

It's assumed you already have [infoportal](https://git.inf.h-brs.de/wagtail/infoportal) on your computer (already brought up the virtual machine (VM) using ```vagrant up``` command).

#### On the host machine

Run these commands on your computer (not from the VM)

- clone this repository inside **infoportal**, and navigate to it. Example:

  ```bash
  ~$ cd /path/to/infoportal
  ~$ git clone git@git.inf.h-brs.de:humari2s/develop_wagtail.git
  ~$ cd develop_wagtail
  ```

- clone our [wagtail_io](https://git.inf.h-brs.de/wagtail/wagtail_io), and navigate to it. Example (cloning using ssh):

  ```bash
  ~$ git clone git@git.inf.h-brs.de:wagtail/wagtail_io.git
  ~$ cd wagtail_io
  ```

- Checkout desired branch . Example:

  ```bash
  ~$ git checkout fb02/2.7.x
  ```

  :warning: Note: don't forget to checkout the correct branch. Otherwise the latest version of wagtail will be installed, which is not the correct one (most likely).

- Navigate back to ```develop_wagtail```, and compile wagtail static files by running this command:

  ```bash
  ~$ cd ..
  ~$ docker-compose up
  ```

  This will build a docker image that has all dependencies needed to compile the static files,  create a docker container, and starts the container for the service which is responsible for compiling the static files.

  The first time you run this command might take longer as it needs to build the docker image.

#### On the Gust Virtual Machine

After successfully compiling the static files (from the host machine), we can install local wagtail python package from inside **infoportal** VM (the guest VM). 

- Bring up the VM, navigate to ```develop_wagtail```, and run ```install.sh``` script. Example:

```bash
[host]~$ cd /path/to/infoportal
[host]~$ vagrant up
...
[host]~$ vagrant ssh
... (logging in to VM)
[infoportal vagrant]~$ cd develop_wagtail
[infoportal vagrant]~$ ./install.sh
```

- ```install.sh``` will install local wagtail package in editable mode, and will overwrite the currently installed wagtail package.
- You can now make changes on ```develop_wagtail/wagtail_io``` from your computer directly (from host), those changes will be reflected directly.

## Finish

After you have made/pushed the changes, and after they are ready to be installed. you can install the released package from our PyPI package server as follows:



- From the VM, run pip install as follows (example for installing version **2.7.2**):

  ```bash
  [infoportal vagrant]~$ pip install --trusted-host pypi.inf.h-brs.de --index-url https://pypi.inf.h-brs.de/simple/ --force-reinstall wagtail==2.7.2
  ```

  

- From the host, you can delete the container as follows:

  ```bash
  [host]~$ docker-compose down
  ```
